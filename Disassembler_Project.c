#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>

//CONVERT INTO BIT PATTERN -Ali Tahririan (Part 1)
void convertNumToBits(char* bits, unsigned short num)
{
	for (int i = 15; i >= 0; i--)
	{
		bits[i] = (num % 2) + '0';
		num /= 2;
	}
	bits[16] = '\0';
}
//COMPUTE NUMBER FROM ABOVE BITS PATTERN-Uyan Tran (Part 2) 
int getNumFromBits(const char* bits, int start, int end)
{
	int coeffPosition = end-start;
	int num = 0;
	while (start<=end)
	{
		if(bits[start]=='1')
			num += pow(2, coeffPosition);
		start++;
		coeffPosition--;
	}
	return num;
}

int main(int argc,char *argv[])
{
	if (argc <= 1)
	{
		printf("INVALID ARGUMENT PRESENT\n");
		exit(1);
	}

	FILE* inFile=fopen(argv[1], "rb");

	if (inFile == NULL)
	{
		printf("INVALID FILE PRESENT\n");
		exit(1);
	}
	
	unsigned short num = -1;
	char bits[17];
	char opcode[4];
	

	while (fread(&num, sizeof(num), 1, inFile))
	{
		//Convert Number To Char-Bits -Ali Tahririan (Part 3)
		convertNumToBits(bits, num);

		//Copying Opcode -Ali Tahririan (Part 3)
		for (int i = 0; i < 3; i++)
			opcode[i] = bits[i];
		opcode[3] ='\0';
		
	//PRINTING FINAL VALUES FOR CODES -SANJUL SHARMA (Part 4)
		//ADD
		if (strcmp(opcode, "000") == 0)
		{
			printf("\t ADD\tR%d, R%d, R%d\n",getNumFromBits(bits,3,5),getNumFromBits(bits,6,8),getNumFromBits(bits,13,15));
		}
		//ADDI
		else if(strcmp(opcode, "001") == 0)
		{
			printf("\t ADDI\tR%d, R%d, %d\n", getNumFromBits(bits, 3, 5), getNumFromBits(bits, 6, 8), getNumFromBits(bits, 9, 15));
		}
		//NAND
		else if (strcmp(opcode, "010") == 0)
		{
			printf("\t NAND\tR%d, R%d, R%d\n", getNumFromBits(bits, 3, 5), getNumFromBits(bits, 6, 8), getNumFromBits(bits, 13, 15));
		}
		//LUI
		else if (strcmp(opcode, "011") == 0)
		{
			printf("\t LUI\tR%d, 0x%X\n", getNumFromBits(bits, 3, 5), getNumFromBits(bits, 6, 15));
		}
		//SW
		else if (strcmp(opcode, "100") == 0)
		{
			printf("\t SW\tR%d, R%d, %d\n", getNumFromBits(bits, 3, 5), getNumFromBits(bits, 6, 8), getNumFromBits(bits, 9, 15));
		}
		//LW
		else if (strcmp(opcode, "101") == 0)
		{
			printf("\t LW\tR%d, R%d, %d\n", getNumFromBits(bits, 3, 5), getNumFromBits(bits, 6, 8), getNumFromBits(bits, 9, 15));
		}
		//BNE
		else if (strcmp(opcode, "110") == 0)
		{
			printf("\t BNE\tR%d, R%d, %d\n", getNumFromBits(bits, 3, 5), getNumFromBits(bits, 6, 8), getNumFromBits(bits, 9, 15));
		}
		//JALR
		else if (strcmp(opcode, "111") == 0)
		{
			printf("\t JALR\tR%d, 0(R%d)\n", getNumFromBits(bits, 3, 5), getNumFromBits(bits, 6, 8));
		}
	//INVALID CODE
		else
		{
			printf("INVALID INSTRUCTION GIVEN\n");
		}

	}
	
	if(inFile)
		fclose(inFile);

	return 0;
}
